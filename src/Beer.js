import React, {Component} from 'react';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import { Button } from '@material-ui/core';
import './Beer.css';

class Beer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            beer: []
        }
    }

    componentDidMount() {
        fetch('https://api.punkapi.com/v2/beers/' + this.props.match.params.beer_id)
            .then(response => response.json())
            .then(data => this.setState({beer: data}));
    }
    /* Render of table beer */
    render() {
        return <div>
            <div id="back">
                <Button variant="outlined" href="/">Back to list</Button>
            </div>
            <h1>Beer Page</h1>

            <Table>
                {this.state.beer.map(param => (
                        <TableBody key={param.id}>
                            <TableRow>
                                <TableCell rowSpan={6} align={"center"}>
                                    {param.image_url === null ? (
                                        <img width={100}
                                             src="https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"
                                             alt="Beer Logo"/>
                                    ) : (
                                        <img width={100} src={param.image_url} alt="Beer Logo"/>
                                    )}
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                {/* Print the name of the beer */}
                                <TableCell>Name :</TableCell>
                                <TableCell>{param.name}</TableCell>
                            </TableRow>
                            <TableRow>
                                {/* Print the description of the beer */}
                                <TableCell>Description :</TableCell>
                                <TableCell>{param.description}</TableCell>
                            </TableRow>
                            <TableRow>
                                {/* Print the first date of brewed */}
                                <TableCell>First Brewed :</TableCell>
                                <TableCell>{param.first_brewed}</TableCell>
                            </TableRow>
                            <TableRow>
                                {/* Print the tips from the brewer */}
                                <TableCell>Brewers Tips :</TableCell>
                                <TableCell>{param.brewers_tips}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Food Pairing :</TableCell>
                                <TableCell>
                                    <ul>
                                        {param.food_pairing.map(option => (
                                            <li key={option}>{option}</li>
                                        ))}
                                    </ul>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    )
                )}
            </Table>
        </div>
    };
}

export default Beer;