import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import {useTheme} from '@material-ui/core/styles';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import {Link} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';


export default function Beers() {
    const [page, setPage] = React.useState(1);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [beers, setBeers] = React.useState([]);


    useEffect(() => {
        api(page, rowsPerPage);
    }, []);

    const api = (page, rowsPerPage) => {
        fetch('https://api.punkapi.com/v2/beers?page=' + page + '&per_page=' + rowsPerPage)
            .then(response => response.json())
            .then(data => setBeers(data));
    };

    /* Initialization constant to change pages */

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
        api(newPage, rowsPerPage);
    };

    /* Initializing constant for change rows per page */

    const handleChangeRowsPerPage = event => {
        const newValue = parseInt(event.target.value, 10);
        setRowsPerPage(newValue);
        setPage(1);
        api(1, newValue);
    };

    /* Initializing constant for autocomplete */

    const handleChangeAutoComplete = event => {
        if(event.target.value === ""){
            api(1, rowsPerPage);
            return;
        }
        fetch('https://api.punkapi.com/v2/beers?beer_name=' + event.target.value)
            .then(response => response.json())
            .then(data => setBeers(data));
    };

    const handleOnCloseAutoComplete = event => {
        api(1, rowsPerPage);
    };

    /* Function return for the table */

    return (<Table>
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={3}>
                            <Autocomplete
                                style={{ width: 300 }}
                                onKeyUp={handleChangeAutoComplete}
                                onAbort={handleOnCloseAutoComplete}
                                options={beers}
                                getOptionLabel={option => option.name}
                                renderInput={params => (
                                    <TextField {...params} label="Search" variant="outlined" fullWidth />
                                )}
                            />
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell> Alcohol by Volume (in %)</TableCell>
                        <TableCell>International Bitterness Unit</TableCell>
                    </TableRow>
                </TableHead>
            <TableBody>
                {beers.map(beer => (
                    <TableRow key={beer.id}>
                        <TableCell>
                            <Link to={'/beer/' + beer.id}>{beer.name}</Link>
                        </TableCell>
                        <TableCell>
                            {beer.abv}°
                        </TableCell>
                        <TableCell>
                            {beer.ibu}
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
            <TableFooter>
                <TableRow>
                    <TablePagination
                        labelDisplayedRows={({page}) => `Page: ${page}`}
                        ActionsComponent={TablePaginationActions}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                        page={page}
                        rowsPerPage={rowsPerPage}
                        rowsPerPageOptions={[10,25,50,80]}
                        count={325}
                    />
                </TableRow>
            </TableFooter>
    </Table>
    );
};

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

/* Event for pagination */

function TablePaginationActions(props) {
    const theme = useTheme();
    const {count, page, rowsPerPage, onChangePage} = props;

    const handleFirstPageButtonClick = event => {
        onChangePage(event, 1);
    };

    const handleBackButtonClick = event => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = event => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = event => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    /* Function return for the pagination */

    return (
        <div>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon/> : <FirstPageIcon/>}
            </IconButton>
            <IconButton onClick={handleBackButtonClick} disabled={page === 1} aria-label="previous page">
                {theme.direction === 'rtl' ? <KeyboardArrowRight/> : <KeyboardArrowLeft/>}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft/> : <KeyboardArrowRight/>}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"

            >
                {theme.direction === 'rtl' ? <FirstPageIcon/> : <LastPageIcon/>}
            </IconButton>
        </div>
    );
}