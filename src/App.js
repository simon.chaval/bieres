import React from 'react';
import './App.css';
import Beers from './Beers';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Beer from "./Beer";

function App() {

  return (
      <Router>
        <div className="App">
            <Route path="/" exact component={Beers}/>
            <Route path="/beer/:beer_id" component={Beer} />
        </div>
      </Router>
  );
}

export default App;
