This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Projet Punk API pour Kasual Business

Fait par Simon CHAVAL

### 'npm start'

Lancez cette commande dans la console du projet

Ouvrez http://localhost:3000 pour le voir dans un navigateur


### Problèmes rencontrés :

Je n'ai pas réussi à limiter le nombre de requête à 1 par seconde pour l'autocomplete et les boutons de paginations.
Je n'ai pas réussi à trier les colonnes de l'alcool par volume et de l'amertume


